```
SVELTE

█▀▀ ▀▄▀ ▄▀█ █▀▄▀█ █▀█ █░░ █▀▀ █▀
██▄ █░█ █▀█ █░▀░█ █▀▀ █▄▄ ██▄ ▄█
```

## Conceptos

```bash
$ git checkout <branch-name>
```

 1. [**Basic-component**](https://gitlab.com/l143/javascript/svelte/1-examples/-/tree/Basic-component)
    - *Script*
    - *Style*
    - *Markdown*
2. [**Component**](https://gitlab.com/l143/javascript/svelte/1-examples/-/tree/Component)
    - *TypeScript*
    - *Preprocessors*
3. [**Data**](https://gitlab.com/l143/javascript/svelte/1-examples/-/tree/Data)
    - *Slots*
    - *Props*
    - *Conditionals (#If)*
    - *Await (#Await)*
    - *Collections (#Each)*
    - *Intenal state*
    - *Reactivity*
4. [**Interaction**](https://gitlab.com/l143/javascript/svelte/1-examples/-/tree/Interaction)
    - *Events*
    - *Bindings*
    - *Lifecycle*
5. [**Store**](https://gitlab.com/l143/javascript/svelte/1-examples/-/tree/Store)
    - *Writtable*
    - *Subscription*
    - *Readable*
    - *Derived*
    - *Custom store*
6. **Extra**
    - *Transitions*

## Dev server
```bash
$ npm run dev
```